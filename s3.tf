resource "aws_s3_bucket" "athena-results" {
  bucket        = var.query_results_s3_bucket_name
  acl           = "private"
  force_destroy = true
}

resource "aws_s3_bucket" "ivr-parameter" {
  bucket        = var.datalake_s3_bucket_name
  acl           = "private"
  force_destroy = true
}

