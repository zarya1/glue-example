variable "datalake_s3_bucket_name" {
  type = string
  default = "cafrjbrdata"
}

variable "query_results_s3_bucket_name" {
  type = string
  default = "cafrjbrresult"
}

variable "datalake_data_prefix" {
  type = string
  default = "cafivrparameter"
}
