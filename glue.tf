resource "aws_glue_catalog_database" "ivrparameterdb" {
  name = "ivrparameterdb"
}

resource "aws_glue_crawler" "ivrparameter_crawler" {
  database_name = aws_glue_catalog_database.ivrparameterdb.name
  name          = "ivrparameter_crawler"
  role          = aws_iam_role.ivr_role.arn

  s3_target {
    path = "s3://${var.datalake_s3_bucket_name}/${var.datalake_data_prefix}/"
  }
}
