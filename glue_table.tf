resource "aws_glue_catalog_table" "aws_glue_catalog_table" {
  name          = var.datalake_data_prefix
  database_name = aws_glue_catalog_database.ivrparameterdb.name
  table_type    = "EXTERNAL_TABLE"

  parameters = {
    EXTERNAL = "TRUE"
  }

  storage_descriptor {
    location      = "s3://${var.datalake_s3_bucket_name}/${var.datalake_data_prefix}/"
    input_format  = "org.apache.hadoop.mapred.TextInputFormat"
    output_format = "org.apache.hadoop.mapred.TextInputFormat"

    ser_de_info {
      name                  = "ivrparameter-serde"
      serialization_library = "org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe"

      parameters = {
        "field.delim"            = ","
        "skip.header.line.count" = "1"
      }
    }

    columns {
      name = "id"
      type = "string"
    }

    columns {
      name = "title"
      type = "string"
    }

  }
}
