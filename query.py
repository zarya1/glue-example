import boto3

client = boto3.client('athena')

response = client.create_named_query(
    Name='TestQuery',
    Database='ivrparameterdb',
    QueryString='select * from ivrparameterdb.cafivrparameter;',
) 
print(response)
